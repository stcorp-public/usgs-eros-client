use std::iter::FromIterator;
use std::str::FromStr;
use std::thread;
use std::time::Duration;

use reqwest::header::{HeaderMap, HeaderName, HeaderValue};
use reqwest::{Client as HttpClient, Url};
use serde::ser::Serialize;
use serde_json::{from_str, to_string};

// The import is actually used, the warning seems to be generated simply
// because the use is behind a compile flag
#[allow(unused_imports)]
use crate::constants::USGS_API_URL;

use crate::constants::{TOKEN_HEADER_NAME, USERAGENT};
use crate::endpoints::{
    DatasetCoverageRequest, DatasetCoverageRequestBuilder, DatasetRequest, DatasetSearchRequest,
    RequestBuilder, SceneSearchBuilder, SceneSearchRequest,
};
use crate::error::{Error, Result};
use crate::types::{ApiData, ApiResponse, Credentials};

#[cfg(test)]
use mockito;

/// USGS EROS M2M API client. This is the main interaction point with the API. The instantiation needs valid
/// user credentials that are used to initiate a session and get an API token. The token is automatically
/// freed when the Client goes out of scope.
///
/// Each Client method then corresponds to an API call and generates a corresponding [RequestBuilder](struct.RequestBuilder.html) object that can
/// be used to build up a request. In order to have the correct builder methods available, the [trait](struct.RequestBuilder.html#trait-implementations) corresponding to
/// the particular request type must be imported. The request can then be called and awaited on.
#[derive(Clone, Debug)]
pub struct Client {
    client: HttpClient,
    base_url: Url,
    session_closed: bool,
    headers: HeaderMap,
}

impl Client {
    /// Instantiate a client session with the given credentials
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::Credentials;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let credentials = Credentials::from_env()?;
    ///    let client = Client::new(&credentials).await?;
    /// #     Ok(())
    /// # }
    /// ```
    pub async fn new(credentials: &Credentials) -> Result<Client> {
        // Log in and get a session token
        #[cfg(not(test))]
        let base_url = Url::parse(USGS_API_URL)?;
        #[cfg(test)]
        let base_url = Url::parse(&mockito::server_url())?;

        let headers = HeaderMap::from_iter(vec![(
            reqwest::header::USER_AGENT,
            HeaderValue::from_str(USERAGENT)?,
        )]);

        let login_url = base_url.join("login")?;
        let str_url = login_url.to_string();
        let client = HttpClient::builder().default_headers(headers).build()?;
        let response = client
            .post(login_url)
            .json(&credentials)
            .send()
            .await?
            .text()
            .await?;

        let json_response = match from_str::<ApiResponse>(&response) {
            Ok(value) => value,
            Err(e) => {
                return Err(Error::UnexpectedResponse {
                    response: response,
                    url: str_url,
                    source: Some(e),
                })
            }
        };

        let token = match json_response.data {
            Some(ApiData::Token(token)) => token,
            other => {
                return Err(Error::UnexpectedResponse {
                    response: to_string(&other)?,
                    url: str_url,
                    source: None,
                })
            }
        };

        let headers = HeaderMap::from_iter(vec![
            (
                reqwest::header::USER_AGENT,
                HeaderValue::from_str(USERAGENT)?,
            ),
            (
                HeaderName::from_str(TOKEN_HEADER_NAME)?,
                HeaderValue::from_str(&token)?,
            ),
        ]);

        let client = HttpClient::builder()
            .default_headers(headers.clone())
            .build()?;

        Ok(Client {
            client,
            base_url,
            session_closed: false,
            headers: headers,
        })
    }

    /// Retrieve a dataset by its id or name
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use usgs_eros_client::{Client, Result};
    /// # use usgs_eros_client::types::Credentials;
    /// use usgs_eros_client::endpoints::DatasetRequestBuilder;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(&credentials).await?;
    ///    let dataset = client
    ///        .dataset()
    ///        .name("lsr_landsat_8_c1")
    ///        .call()
    ///        .await?;
    /// #     Ok(())
    /// # }
    /// ```
    pub fn dataset(&self) -> RequestBuilder<'_, DatasetRequest> {
        RequestBuilder::new(self, "dataset")
    }

    /// Retrieve dataset coverage by name
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use usgs_eros_client::{Client, Result};
    /// # use usgs_eros_client::types::Credentials;
    /// use usgs_eros_client::endpoints::DatasetCoverageRequestBuilder;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(&credentials).await?;
    ///    let dataset_coverage = client
    ///        .dataset_coverage("lsr_landsat_8_c1")
    ///        .call()
    ///        .await?;
    /// #     Ok(())
    /// # }
    /// ```
    pub fn dataset_coverage(
        &self,
        dataset_name: &str,
    ) -> RequestBuilder<'_, DatasetCoverageRequest> {
        let mut builder: RequestBuilder<'_, DatasetCoverageRequest> =
            RequestBuilder::new(self, "dataset-coverage");
        builder.name(dataset_name);
        return builder;
    }

    /// Search available datasets
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use usgs_eros_client::{Client, Result};
    /// # use usgs_eros_client::types::Credentials;
    /// use usgs_eros_client::endpoints::DatasetSearchBuilder;
    /// use usgs_eros_client::types::{Coordinate, DateRange, SpatialFilter};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(&credentials).await?;
    ///    let lower_left = Coordinate::new(-10.5, 10.5).unwrap();
    ///    let upper_right = Coordinate::new(-10.5, 10.5).unwrap();
    ///    let date_range = DateRange::new("2014-05-01", "2014-05-30").unwrap();
    ///
    ///    let search_result = client
    ///        .dataset_search()
    ///        .dataset_name("lsr_landsat")
    ///        .temporal_filter(date_range)
    ///        .spatial_filter(SpatialFilter::mbr(lower_left, upper_right))
    ///        .call()
    ///        .await?;
    /// #     Ok(())
    /// # }
    pub fn dataset_search(&self) -> RequestBuilder<'_, DatasetSearchRequest> {
        RequestBuilder::new(self, "dataset-search")
    }

    /// Search for scenes
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use usgs_eros_client::{Client, Result};
    /// # use usgs_eros_client::types::Credentials;
    /// use std::path::Path;
    /// use usgs_eros_client::endpoints::SceneSearchBuilder;
    /// use usgs_eros_client::types::{TemporalFilter, CloudCoverFilter, GeoJson, SpatialFilter, MetadataFilter, SortDirection, MetadataType};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(&credentials).await?;
    ///    let acquisition_filter = TemporalFilter::new("2018-11-30", "2018-12-30").unwrap();
    ///    let cloud_filter = CloudCoverFilter::new(0, 100, true);
    ///    let geojson = GeoJson::from_file(Path::new("test-data/geojson-filter.json")).unwrap();
    ///    let spatial_filter = SpatialFilter::geojson(geojson);
    ///    let meta1 = MetadataFilter::between("5e7c418226384db5", 10, 50);
    ///    let meta2 = MetadataFilter::between("5e7c4182b7148b69", 50, 90);
    ///    let meta_filter = MetadataFilter::and(vec![meta1, meta2]);
    ///    let search_result = client
    ///        .scene_search("lsr_landsat_8_c1")
    ///        .max_results(20)
    ///        .starting_number(1)
    ///        .sort_direction(SortDirection::ASC)
    ///        .metadata_type(MetadataType::Summary)
    ///        .acquisition_filter(acquisition_filter)
    ///        .cloud_cover_filter(cloud_filter)
    ///        .spatial_filter(spatial_filter)
    ///        .metadata_filter(meta_filter)
    ///        .call()
    ///        .await?;
    /// #     Ok(())
    /// # }
    pub fn scene_search(&self, dataset_name: &str) -> RequestBuilder<'_, SceneSearchRequest> {
        let mut builder: RequestBuilder<'_, SceneSearchRequest> =
            RequestBuilder::new(self, "scene-search");
        builder.dataset_name(dataset_name);
        return builder;
    }

    /// Explicitly close the given session by logging out and releasing the token
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use usgs_eros_client::{Client, Result};
    /// # use usgs_eros_client::types::Credentials;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// let credentials = Credentials::from_env()?;
    /// let client = Client::new(&credentials).await?;
    /// client.close_session().await?;
    /// // The client has been consumed here
    /// #     Ok(())
    /// # }
    pub async fn close_session(mut self) -> Result<ApiResponse> {
        self.session_closed = true;
        let response = self.get("logout", None::<&()>).await?;
        Ok(response)
    }

    /// Perform a GET request
    pub(crate) async fn get<S: Serialize>(
        &self,
        endpoint: &str,
        body: Option<&S>,
    ) -> Result<ApiResponse> {
        let call_url = self.base_url.join(endpoint)?;
        let str_url = call_url.to_string();
        let response = self
            .client
            .get(call_url)
            .json(&body)
            .send()
            .await?
            .text()
            .await?;

        let json_response = match from_str::<ApiResponse>(&response) {
            Ok(value) => value,
            Err(e) => {
                return Err(Error::UnexpectedResponse {
                    response: response,
                    url: str_url,
                    source: Some(e),
                })
            }
        };

        Ok(json_response)
    }
}

impl Drop for Client {
    /// Release the token automatically when Client goes out of scope.
    fn drop(&mut self) {
        if self.session_closed == false {
            self.session_closed = true;
            let logout_url = match self.base_url.join("logout") {
                Ok(url) => url,
                _ => return (),
            };

            let headers = self.headers.clone();

            let thread = thread::spawn(move || {
                let client = match reqwest::blocking::Client::builder().build() {
                    Ok(cl) => cl,
                    _ => return (),
                };

                // 1 second
                let timeout = Duration::new(1, 0);

                match client
                    .get(logout_url)
                    .headers(headers)
                    .timeout(timeout)
                    .send()
                {
                    _ => (),
                }
            });
            thread.join().unwrap();
        }
    }
}

// Don't try to move tests to their own module. This will make them into
// integration tests and invalidate the #[cfg(test)] compile flag used
// to use the mockito server URL. E.g., it will actually try to call
// the actual API URL when running `cargo test`.USGS_API_URL
//
// See also the relevant issue: https://github.com/rust-lang/rust/issues/45599
#[cfg(test)]
mod test {
    use crate::endpoints::{DatasetRequestBuilder, DatasetSearchBuilder, SceneSearchBuilder};
    use crate::Client;

    use crate::types::{
        ApiData, CloudCoverFilter, Coordinate, Credentials, DateRange, GeoJson, MetadataFilter,
        MetadataType, SortDirection, SpatialFilter, TemporalFilter,
    };

    use mockito::mock;
    use std::fs::read_to_string;
    use std::path::Path;

    #[tokio::test]
    async fn test_nominal() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials {
            username: "Alice_P_Hacker".to_owned(),
            password: "pass123".to_owned(),
        };

        Client::new(&credentials).await.unwrap();
    }

    #[tokio::test]
    async fn test_dataset() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials::new("Alice_P_Hacker", "pass123");
        let client = Client::new(&credentials).await.unwrap();

        let res = client
            .dataset()
            .name("lsr_landsat_8_c1")
            .call()
            .await
            .unwrap();

        match res.data {
            Some(ApiData::Dataset(_)) => (),
            _ => panic!("Incorrect return variant"),
        }
    }

    #[tokio::test]
    async fn test_dataset_coverage() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials::new("Alice_P_Hacker", "pass123");
        let client = Client::new(&credentials).await.unwrap();

        match client
            .dataset_coverage("lsr_landsat_8_c1")
            .call()
            .await
            .unwrap()
            .data
        {
            Some(ApiData::DatasetCoverage(_)) => (),
            _ => panic!("Incorrect return variant"),
        }
    }

    #[tokio::test]
    async fn test_dataset_search() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials::new("Alice_P_Hacker", "pass123");
        let client = Client::new(&credentials).await.unwrap();
        let lower_left = Coordinate::new(-10.5, 10.5).unwrap();
        let upper_right = Coordinate::new(-10.5, 10.5).unwrap();
        let date_range = DateRange::new("2014-05-01", "2014-05-30").unwrap();

        let res = client
            .dataset_search()
            .dataset_name("lsr_landsat")
            .temporal_filter(date_range)
            .spatial_filter(SpatialFilter::mbr(lower_left, upper_right))
            .call()
            .await
            .unwrap();

        match res.data {
            Some(ApiData::Datasets(_)) => (),
            _ => panic!("Incorrect return variant"),
        }
    }

    #[tokio::test]
    async fn test_scene_search_simple() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials::new("Alice_P_Hacker", "pass123");
        let client = Client::new(&credentials).await.unwrap();

        let res = client
            .scene_search("lsr_landsat_8_c1")
            .max_results(3)
            .call()
            .await
            .unwrap();

        match res.data {
            Some(ApiData::SceneSearch(_)) => (),
            _ => panic!("Incorrect return variant"),
        }
    }

    #[tokio::test]
    async fn test_scene_search_involved() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials::new("Alice_P_Hacker", "pass123");
        let client = Client::new(&credentials).await.unwrap();

        let acquisition_filter = TemporalFilter::new("2018-11-30", "2018-12-30").unwrap();
        let cloud_filter = CloudCoverFilter::new(0, 100, true);
        let geojson = GeoJson::from_file(Path::new("test-data/geojson-filter.json")).unwrap();
        let spatial_filter = SpatialFilter::geojson(geojson);
        let meta1 = MetadataFilter::between("5e7c418226384db5", 10, 50);
        let meta2 = MetadataFilter::between("5e7c4182b7148b69", 50, 90);
        let meta_filter = MetadataFilter::and(vec![meta1, meta2]);
        let res = client
            .scene_search("lsr_landsat_8_c1")
            .max_results(20)
            .starting_number(1)
            .sort_direction(SortDirection::ASC)
            .metadata_type(MetadataType::Summary)
            .acquisition_filter(acquisition_filter)
            .cloud_cover_filter(cloud_filter)
            .spatial_filter(spatial_filter)
            .metadata_filter(meta_filter)
            .call()
            .await
            .unwrap();

        match res.data {
            Some(ApiData::SceneSearch(_)) => (),
            _ => panic!("Incorrect return variant"),
        }
    }

    fn mock_server_setup() -> Vec<mockito::Mock> {
        let mut login_request = read_to_string("test-data/login-request.json").unwrap();
        login_request.retain(|c| c != ' ');
        login_request.retain(|c| c != '\n');

        let mut ds_request = read_to_string("test-data/dataset-request.json").unwrap();
        ds_request.retain(|c| c != ' ');
        ds_request.retain(|c| c != '\n');

        let mut dscov_request = read_to_string("test-data/dataset-coverage-request.json").unwrap();
        dscov_request.retain(|c| c != ' ');
        dscov_request.retain(|c| c != '\n');

        let mut dssearch_request = read_to_string("test-data/dataset-search-request.json").unwrap();
        dssearch_request.retain(|c| c != ' ');
        dssearch_request.retain(|c| c != '\n');

        let mut scene_search_simple_request =
            read_to_string("test-data/scene-search-simple-request.json").unwrap();
        scene_search_simple_request.retain(|c| c != ' ');
        scene_search_simple_request.retain(|c| c != '\n');

        let mut scene_search_involved_request =
            read_to_string("test-data/scene-search-involved-request.json").unwrap();
        scene_search_involved_request.retain(|c| c != ' ');
        scene_search_involved_request.retain(|c| c != '\n');

        let login = mock("POST", "/login")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .with_body_from_file("test-data/login-response.json")
            .match_body(login_request.as_str())
            .create();

        let logout = mock("GET", "/logout")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .match_header(
                "x-auth-token",
                "eyJjaWQiOjE3MzAwOTAsInMiOiIxNjAxNTc5NDQ1IiwiciI6ODEyLCJwIjpbXX0=",
            )
            .with_body_from_file("test-data/logout-response.json")
            .create();

        let dataset = mock("GET", "/dataset")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .match_header(
                "x-auth-token",
                "eyJjaWQiOjE3MzAwOTAsInMiOiIxNjAxNTc5NDQ1IiwiciI6ODEyLCJwIjpbXX0=",
            )
            .match_body(ds_request.as_str())
            .with_body_from_file("test-data/dataset-response.json")
            .create();

        let dataset_coverage = mock("GET", "/dataset-coverage")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .match_header(
                "x-auth-token",
                "eyJjaWQiOjE3MzAwOTAsInMiOiIxNjAxNTc5NDQ1IiwiciI6ODEyLCJwIjpbXX0=",
            )
            .match_body(dscov_request.as_str())
            .with_body_from_file("test-data/dataset-coverage-response.json")
            .create();

        let dataset_search = mock("GET", "/dataset-search")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .match_header(
                "x-auth-token",
                "eyJjaWQiOjE3MzAwOTAsInMiOiIxNjAxNTc5NDQ1IiwiciI6ODEyLCJwIjpbXX0=",
            )
            .match_body(dssearch_request.as_str())
            .with_body_from_file("test-data/dataset-search-response.json")
            .create();

        let scene_search_simple = mock("GET", "/scene-search")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .match_header(
                "x-auth-token",
                "eyJjaWQiOjE3MzAwOTAsInMiOiIxNjAxNTc5NDQ1IiwiciI6ODEyLCJwIjpbXX0=",
            )
            .match_body(scene_search_simple_request.as_str())
            .with_body_from_file("test-data/scene-search-simple-response.json")
            .create();

        let scene_search_involved = mock("GET", "/scene-search")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .match_header(
                "x-auth-token",
                "eyJjaWQiOjE3MzAwOTAsInMiOiIxNjAxNTc5NDQ1IiwiciI6ODEyLCJwIjpbXX0=",
            )
            .match_body(scene_search_involved_request.as_str())
            .with_body_from_file("test-data/scene-search-involved-response.json")
            .create();

        return vec![
            login,
            logout,
            dataset,
            dataset_coverage,
            dataset_search,
            scene_search_simple,
            scene_search_involved,
        ];
    }
}
