use crate::endpoints::RequestBuilder;
use crate::types::{
    CloudCoverFilter, MetadataFilter, MetadataType, Scene, SceneFilter, SortDirection,
    SpatialFilter, TemporalFilter,
};
use serde::{Deserialize, Serialize};

/// [Scene search](https://m2m.cr.usgs.gov/api/docs/reference/#scene-search) request
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SceneSearchRequest {
    dataset_name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    max_results: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    starting_number: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    metadata_type: Option<MetadataType>,
    #[serde(skip_serializing_if = "Option::is_none")]
    sort_field: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    sort_direction: Option<SortDirection>,
    #[serde(skip_serializing_if = "Option::is_none")]
    scene_filter: Option<SceneFilter>,
    #[serde(skip_serializing_if = "Option::is_none")]
    compare_list_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    bulk_list_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    order_list_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    exclude_list_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    include_null_metadata_values: Option<bool>,
}

impl Default for SceneSearchRequest {
    fn default() -> SceneSearchRequest {
        SceneSearchRequest {
            dataset_name: "".to_owned(),
            max_results: Some(100),
            starting_number: None,
            metadata_type: None,
            sort_field: None,
            sort_direction: None,
            scene_filter: None,
            compare_list_name: None,
            bulk_list_name: None,
            order_list_name: None,
            exclude_list_name: None,
            include_null_metadata_values: None,
        }
    }
}

/// [Scene search](https://m2m.cr.usgs.gov/api/docs/reference/#scene-search) result
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SceneSearchResult {
    pub results: Vec<Scene>,
    pub total_hits: u64,
    pub next_record: u64,
    pub num_excluded: u64,
    pub records_returned: u16,
    pub starting_number: u64,
}

/// Scene search builder methods
///
/// For usage example see the [Client::scene_search](../struct.Client.html#method.scene_search) method
pub trait SceneSearchBuilder {
    fn dataset_name(&mut self, name: &str) -> &mut Self;
    fn max_results(&mut self, max_results: u16) -> &mut Self;
    fn starting_number(&mut self, starting_number: u16) -> &mut Self;
    fn metadata_type(&mut self, metadata_type: MetadataType) -> &mut Self;
    fn sort_field(&mut self, sort_field: &str) -> &mut Self;
    fn sort_direction(&mut self, sort_direction: SortDirection) -> &mut Self;
    fn scene_filter(&mut self, filter: SceneFilter) -> &mut Self;
    fn compare_list_name(&mut self, name: &str) -> &mut Self;
    fn bulk_list_name(&mut self, name: &str) -> &mut Self;
    fn order_list_name(&mut self, name: &str) -> &mut Self;
    fn exclude_list_name(&mut self, name: &str) -> &mut Self;
    fn include_null_metadata_values(&mut self, include_values: bool) -> &mut Self;
    fn acquisition_filter(&mut self, filter: TemporalFilter) -> &mut Self;
    fn cloud_cover_filter(&mut self, filter: CloudCoverFilter) -> &mut Self;
    fn ingest_filter(&mut self, filter: TemporalFilter) -> &mut Self;
    fn metadata_filter(&mut self, filter: MetadataFilter) -> &mut Self;
    fn seasonal_filter(&mut self, filter: Vec<u8>) -> &mut Self;
    fn spatial_filter(&mut self, filter: SpatialFilter) -> &mut Self;
}

impl SceneSearchBuilder for RequestBuilder<'_, SceneSearchRequest> {
    fn dataset_name(&mut self, name: &str) -> &mut Self {
        self.request.dataset_name = name.to_owned();
        return self;
    }

    fn max_results(&mut self, max_results: u16) -> &mut Self {
        self.request.max_results = Some(max_results);
        return self;
    }

    fn starting_number(&mut self, starting_number: u16) -> &mut Self {
        self.request.starting_number = Some(starting_number);
        return self;
    }

    fn metadata_type(&mut self, metadata_type: MetadataType) -> &mut Self {
        self.request.metadata_type = Some(metadata_type);
        return self;
    }

    fn sort_field(&mut self, sort_field: &str) -> &mut Self {
        self.request.sort_field = Some(sort_field.to_owned());
        return self;
    }

    fn sort_direction(&mut self, sort_direction: SortDirection) -> &mut Self {
        self.request.sort_direction = Some(sort_direction);
        return self;
    }

    fn scene_filter(&mut self, filter: SceneFilter) -> &mut Self {
        self.request.scene_filter = Some(filter);
        return self;
    }

    fn compare_list_name(&mut self, name: &str) -> &mut Self {
        self.request.compare_list_name = Some(name.to_owned());
        return self;
    }

    fn bulk_list_name(&mut self, name: &str) -> &mut Self {
        self.request.bulk_list_name = Some(name.to_owned());
        return self;
    }

    fn order_list_name(&mut self, name: &str) -> &mut Self {
        self.request.order_list_name = Some(name.to_owned());
        return self;
    }

    fn exclude_list_name(&mut self, name: &str) -> &mut Self {
        self.request.exclude_list_name = Some(name.to_owned());
        return self;
    }

    fn include_null_metadata_values(&mut self, include_values: bool) -> &mut Self {
        self.request.include_null_metadata_values = Some(include_values);
        return self;
    }

    fn acquisition_filter(&mut self, filter: TemporalFilter) -> &mut Self {
        let scene_filter = self.request.scene_filter.get_or_insert(SceneFilter::new());
        scene_filter.acquisition_filter(filter);
        return self;
    }

    fn cloud_cover_filter(&mut self, filter: CloudCoverFilter) -> &mut Self {
        let scene_filter = self.request.scene_filter.get_or_insert(SceneFilter::new());
        scene_filter.cloud_cover_filter(filter);
        return self;
    }

    fn ingest_filter(&mut self, filter: TemporalFilter) -> &mut Self {
        let scene_filter = self.request.scene_filter.get_or_insert(SceneFilter::new());
        scene_filter.ingest_filter(filter);
        return self;
    }

    fn metadata_filter(&mut self, filter: MetadataFilter) -> &mut Self {
        let scene_filter = self.request.scene_filter.get_or_insert(SceneFilter::new());
        scene_filter.metadata_filter(filter);
        return self;
    }

    fn seasonal_filter(&mut self, filter: Vec<u8>) -> &mut Self {
        let scene_filter = self.request.scene_filter.get_or_insert(SceneFilter::new());
        scene_filter.seasonal_filter(filter);
        return self;
    }

    fn spatial_filter(&mut self, filter: SpatialFilter) -> &mut Self {
        let scene_filter = self.request.scene_filter.get_or_insert(SceneFilter::new());
        scene_filter.spatial_filter(filter);
        return self;
    }
}
