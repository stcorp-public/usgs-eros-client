use serde::{Deserialize, Serialize};

use crate::endpoints::RequestBuilder;
use crate::types::GeoJson;

/// [Dataset coverage](https://m2m.cr.usgs.gov/api/docs/reference/#dataset-coverage) request
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DatasetCoverageRequest {
    pub dataset_name: String,
}

impl Default for DatasetCoverageRequest {
    fn default() -> DatasetCoverageRequest {
        DatasetCoverageRequest {
            dataset_name: "".to_owned(),
        }
    }
}

/// Dataset coverage request builder methods
///
/// For usage example see the [Client::dataset_coverage](../struct.Client.html#method.dataset_coverage) method
pub trait DatasetCoverageRequestBuilder {
    /// Use datasetName to construct the request
    fn name(&mut self, dataset_name: &str) -> &mut Self;
}

impl DatasetCoverageRequestBuilder for RequestBuilder<'_, DatasetCoverageRequest> {
    /// Use datasetName to construct the request
    fn name(&mut self, dataset_name: &str) -> &mut Self {
        self.request.dataset_name = dataset_name.to_owned();
        return self;
    }
}

/// [Dataset coverage](https://m2m.cr.usgs.gov/api/docs/reference/#dataset-coverage) result
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DatasetCoverageResult {
    pub bounds: GeoJson,
    pub geo_json: GeoJson,
}
