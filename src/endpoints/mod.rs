//! API endpoint implementation
//!
//! There's a general [RequestBuilder](struct.RequestBuilder.html) used to instantiate and build
//! a request, as well as perform an API call.
//!
//! Implementation of a particular endpoint consists of:
//!
//! * A type representing the request
//! * A type representing the response data, in case it can not be constructed
//!   directly from API [types](../types/index.html).
//! * A trait implementing the methods needed for building the particular request
//! * A method on the [Client](../struct.Client.html) for using the API endpoint
//!
use crate::types::ApiResponse;
use crate::{Client, Result};
use serde::Serialize;

mod dataset;
mod dataset_coverage;
mod dataset_search;
mod scene_search;

pub use dataset::*;
pub use dataset_coverage::*;
pub use dataset_search::*;
pub use scene_search::*;

/// Generic RequestBuilder used to build and call all endpoints
pub struct RequestBuilder<'a, T: Serialize> {
    request: T,
    endpoint: String,
    client: &'a Client,
}

impl<T: Serialize + Default> RequestBuilder<'_, T> {
    /// Instantiate a request builder
    pub fn new<'a>(client: &'a Client, endpoint: &str) -> RequestBuilder<'a, T> {
        let request = T::default();
        let endpoint = endpoint.to_owned();

        RequestBuilder {
            request,
            endpoint,
            client,
        }
    }

    /// Call the request
    pub async fn call(&self) -> Result<ApiResponse> {
        self.client.get(&self.endpoint, Some(&self.request)).await
    }
}
