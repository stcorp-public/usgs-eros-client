use serde::{Deserialize, Serialize};

use crate::endpoints::RequestBuilder;
use crate::error::Result;
use crate::types::{Coordinate, DateRange, SpatialFilter};

/// [Dataset search](https://m2m.cr.usgs.gov/api/docs/reference/#dataset-search) request
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DatasetSearchRequest {
    #[serde(skip_serializing_if = "Option::is_none")]
    catalog: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    category_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    dataset_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    include_messages: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    public_only: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    include_unknown_spatial: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    temporal_filter: Option<DateRange>,
    #[serde(skip_serializing_if = "Option::is_none")]
    spatial_filter: Option<SpatialFilter>,
}

impl Default for DatasetSearchRequest {
    fn default() -> DatasetSearchRequest {
        DatasetSearchRequest {
            catalog: None,
            category_id: None,
            dataset_name: None,
            include_messages: None,
            public_only: None,
            include_unknown_spatial: None,
            temporal_filter: None,
            spatial_filter: None,
        }
    }
}

/// Dataset search builder methods
///
/// For usage example see the [Client::dataset_search](../struct.Client.html#method.dataset_search) method
pub trait DatasetSearchBuilder {
    fn catalog(&mut self, catalog: &str) -> &mut Self;
    fn category_id(&mut self, category_id: &str) -> &mut Self;
    fn dataset_name(&mut self, dataset_name: &str) -> &mut Self;
    fn include_messages(&mut self, include_messages: bool) -> &mut Self;
    fn public_only(&mut self, public_only: bool) -> &mut Self;
    fn include_unknown_spatial(&mut self, include_unknown_spatial: bool) -> &mut Self;
    fn temporal_filter(&mut self, temporal_filter: DateRange) -> &mut Self;
    fn temporal_filter_str(&mut self, start_date: &str, end_date: &str) -> Result<&mut Self>;
    fn spatial_filter(&mut self, spatial_filter: SpatialFilter) -> &mut Self;
    fn spatial_filter_bounds(
        &mut self,
        lower_left: (f64, f64),
        upper_right: (f64, f64),
    ) -> Result<&mut Self>;
}

impl DatasetSearchBuilder for RequestBuilder<'_, DatasetSearchRequest> {
    fn catalog(&mut self, catalog: &str) -> &mut Self {
        self.request.catalog = Some(catalog.to_owned());
        return self;
    }

    fn category_id(&mut self, category_id: &str) -> &mut Self {
        self.request.category_id = Some(category_id.to_owned());
        return self;
    }

    fn dataset_name(&mut self, dataset_name: &str) -> &mut Self {
        self.request.dataset_name = Some(dataset_name.to_owned());
        return self;
    }

    fn include_messages(&mut self, include_messages: bool) -> &mut Self {
        self.request.include_messages = Some(include_messages);
        return self;
    }

    fn public_only(&mut self, public_only: bool) -> &mut Self {
        self.request.public_only = Some(public_only);
        return self;
    }

    fn include_unknown_spatial(&mut self, include_unknown_spatial: bool) -> &mut Self {
        self.request.include_unknown_spatial = Some(include_unknown_spatial);
        return self;
    }

    fn temporal_filter(&mut self, temporal_filter: DateRange) -> &mut Self {
        self.request.temporal_filter = Some(temporal_filter);
        return self;
    }

    fn temporal_filter_str(&mut self, start_date: &str, end_date: &str) -> Result<&mut Self> {
        let temporal_filter = DateRange::new(start_date, end_date)?;
        self.request.temporal_filter = Some(temporal_filter);
        return Ok(self);
    }

    fn spatial_filter(&mut self, spatial_filter: SpatialFilter) -> &mut Self {
        self.request.spatial_filter = Some(spatial_filter);
        return self;
    }

    fn spatial_filter_bounds(
        &mut self,
        lower_left: (f64, f64),
        upper_right: (f64, f64),
    ) -> Result<&mut Self> {
        let lower_left = Coordinate::new(lower_left.0, lower_left.1)?;
        let upper_right = Coordinate::new(upper_right.0, upper_right.1)?;
        let spatial_filter = SpatialFilter::mbr(lower_left, upper_right);
        self.request.spatial_filter = Some(spatial_filter);
        return Ok(self);
    }
}
