//! Dataset endpoint implementation
//!
use serde::{Deserialize, Serialize};

use crate::endpoints::RequestBuilder;

/// [Dataset](https://m2m.cr.usgs.gov/api/docs/reference/#dataset) request
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum DatasetRequest {
    DatasetId(String),
    DatasetName(String),
}

impl Default for DatasetRequest {
    fn default() -> DatasetRequest {
        DatasetRequest::DatasetName("".to_owned())
    }
}

/// Dataset request builder methods
///
/// For usage example see the [Client::dataset](../struct.Client.html#method.dataset) method
pub trait DatasetRequestBuilder {
    /// Use datasetId to construct the request
    fn id(&mut self, dataset_id: &str) -> &mut Self;

    /// Use datasetName to construct the request
    fn name(&mut self, dataset_name: &str) -> &mut Self;
}

impl DatasetRequestBuilder for RequestBuilder<'_, DatasetRequest> {
    /// Use datasetId to construct the request
    fn id(&mut self, dataset_id: &str) -> &mut Self {
        self.request = DatasetRequest::DatasetId(dataset_id.to_owned());
        return self;
    }

    /// Use datasetName to construct the request
    fn name(&mut self, dataset_name: &str) -> &mut Self {
        self.request = DatasetRequest::DatasetName(dataset_name.to_owned());
        return self;
    }
}
