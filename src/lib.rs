//! An API client for [USGS EROS M2M](https://m2m.cr.usgs.gov/api/docs)
//!
//! # Example
//!
//! Cargo.toml:
//! ```toml
//! [dependencies]
//!usgs-eros-client = "^1.0"
//!tokio = {version = "^0.2", features = ["macros"]}
//! ```
//!
//! Basic functionality:
//! ```no_run
//! use usgs_eros_client::{Client, Result};
//! use usgs_eros_client::types::Credentials;
//! use usgs_eros_client::endpoints::DatasetRequestBuilder;
//!
//! #[tokio::main]
//!async fn main() -> Result<()> {
//!    let credentials = Credentials::from_env()?;
//!    let client = Client::new(&credentials).await?;
//!    let dataset = client.dataset()
//!        .name("gls_all").call().await?;
//!    println!("Dataset response: {:?}", dataset);
//!    Ok(())
//!}
//! ```
//!
//! For implemented API endpoints, see the methods implemented on the [Client](struct.Client.html#impl)

mod client;
mod constants;
mod error;

pub mod endpoints;
pub mod types;

pub use client::Client;
pub use error::{Error, Result};
// Make sure the relevant consts appear in documentation
pub use constants::{ENVVAR_PASS, ENVVAR_USER, USGS_API_URL};
