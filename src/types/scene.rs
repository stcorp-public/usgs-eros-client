use serde::{Deserialize, Serialize};

use crate::types::{DateRange, SpatialBounds};

/// A single [scene](https://m2m.cr.usgs.gov/api/docs/datatypes/#scene)
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Scene {
    pub browse: Vec<Browse>,
    pub cloud_cover: Option<String>,
    pub entity_id: String,
    pub display_id: String,
    pub ordering_id: Option<String>,
    pub metadata: Vec<MetadataField>,
    pub options: Options,
    pub selected: Selected,
    pub spatial_bounds: SpatialBounds,
    pub spatial_coverage: SpatialBounds,
    pub temporal_coverage: DateRange,
    pub publish_date: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Browse {
    pub browse_rotation_enabled: Option<bool>,
    pub browse_name: String,
    pub browse_path: String,
    pub overlay_path: String,
    pub overlay_type: String,
    pub thumbnail_path: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Options {
    pub bulk: bool,
    pub order: bool,
    pub download: bool,
    pub secondary: bool,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Selected {
    pub bulk: bool,
    pub order: bool,
    pub compare: bool,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MetadataField {
    pub id: String,
    pub value: MetadataValue,
    pub field_name: String,
    pub dictionary_link: Option<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum MetadataValue {
    Str(String),
    Int(i64),
}
