use std::env::var;

use serde::{Deserialize, Serialize};

use crate::constants::{ENVVAR_PASS, ENVVAR_USER};
use crate::error::Error;
use crate::Result;

#[derive(Clone, Debug, Serialize, Deserialize)]
/// USGS ERS access credentials
pub struct Credentials {
    pub username: String,
    pub password: String,
}

impl Credentials {
    /// Read the access credentials from [environment variables](index.html#constants)
    pub fn from_env() -> Result<Credentials> {
        let username =
            var(ENVVAR_USER).map_err(|_| Error::EnvironmentError(ENVVAR_USER.to_string()))?;
        let password =
            var(ENVVAR_PASS).map_err(|_| Error::EnvironmentError(ENVVAR_PASS.to_string()))?;

        Ok(Credentials { username, password })
    }

    /// Convience constructor method
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// # use usgs_eros_client::types::Credentials;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///   let credentials = Credentials::new("Alice_P_Hacker", "pass123");
    /// #     Ok(())
    /// # }
    pub fn new(username: &str, password: &str) -> Credentials {
        Credentials {
            username: username.to_owned(),
            password: password.to_owned(),
        }
    }
}
