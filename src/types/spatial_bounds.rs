use std::fs::read_to_string;
use std::path::Path;

use serde::{Deserialize, Serialize};
use serde_json::from_str;

use crate::Result;

/// A [spatialBounds](https://m2m.cr.usgs.gov/api/docs/datatypes/#spatialBounds) object
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum SpatialBounds {
    SpatialBoundsMbr {
        north: f64,
        east: f64,
        south: f64,
        west: f64,
    },
    GeoJson(GeoJson),
}

/// A geojson representation
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "type", content = "coordinates")]
pub enum GeoJson {
    Polygon(Vec<Vec<RawCoordinate>>),
    MultiPolygon(Vec<Vec<Vec<RawCoordinate>>>),
}

impl GeoJson {
    /// Read a geojson representation from a file
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use usgs_eros_client::{Client, Result};
    /// use std::path::Path;
    /// use usgs_eros_client::types::GeoJson;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let geojson = GeoJson::from_file(Path::new("geojson.json")).unwrap();
    /// #     Ok(())
    /// # }
    pub fn from_file(path: &Path) -> Result<GeoJson> {
        let geojson: GeoJson = from_str(&read_to_string(path)?)?;
        Ok(geojson)
    }
}

type RawCoordinate = (f64, f64);
