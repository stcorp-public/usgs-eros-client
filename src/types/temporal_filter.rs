use std::str::FromStr;

use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

use crate::Result;

/// TemporalFilter stands in for AcquisitionFilter and IngestionFilter
/// from the docs, as both of those have exactly the same signature.
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TemporalFilter {
    start: NaiveDate,
    end: NaiveDate,
}

impl TemporalFilter {
    /// Construct a temporal filter from ISO 8601 formatted time strings
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::Result;
    /// # use usgs_eros_client::types::TemporalFilter;
    /// #
    /// # fn main() -> Result<()> {
    /// let temporal_filter = TemporalFilter::new("2010-12-30", "2011-12-30")?;
    /// #
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// # Errors
    ///
    /// The constructor errors in case the provided strings can not be parsed into dates.
    pub fn new(start: &str, end: &str) -> Result<TemporalFilter> {
        let start = NaiveDate::from_str(start)?;
        let end = NaiveDate::from_str(end)?;
        Ok(TemporalFilter { start, end })
    }
}
