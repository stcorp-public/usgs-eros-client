use serde::{Deserialize, Serialize};

use crate::types::{CloudCoverFilter, MetadataFilter, SpatialFilter, TemporalFilter};

/// A [scene filter](https://m2m.cr.usgs.gov/api/docs/datatypes/#sceneFilter) used to define and narrow a scene search
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SceneFilter {
    acquisition_filter: Option<TemporalFilter>,
    cloud_cover_filter: Option<CloudCoverFilter>,
    #[serde(skip_serializing_if = "Option::is_none")]
    dataset_name: Option<String>,
    ingest_filter: Option<TemporalFilter>,
    metadata_filter: Option<MetadataFilter>,
    #[serde(skip_serializing_if = "Option::is_none")]
    seasonal_filter: Option<Vec<u8>>,
    spatial_filter: Option<SpatialFilter>,
}

impl SceneFilter {
    /// Instantiate an empty scene filter
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::SceneFilter;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let scene_filter = SceneFilter::new();
    /// #     Ok(())
    /// # }
    pub fn new() -> SceneFilter {
        SceneFilter {
            acquisition_filter: None,
            cloud_cover_filter: None,
            dataset_name: None,
            ingest_filter: None,
            metadata_filter: None,
            seasonal_filter: None,
            spatial_filter: None,
        }
    }

    /// Add or change an [acquisition filter](https://m2m.cr.usgs.gov/api/docs/datatypes/#acquisitionFilter)
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::{SceneFilter, TemporalFilter};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let mut scene_filter = SceneFilter::new();
    ///    let acquisition_filter = TemporalFilter::new("2018-11-30", "2018-12-30").unwrap();
    ///    scene_filter.acquisition_filter(acquisition_filter);
    /// #     Ok(())
    /// # }
    pub fn acquisition_filter(&mut self, filter: TemporalFilter) -> &mut Self {
        self.acquisition_filter = Some(filter);
        return self;
    }

    /// Add or change a [cloud cover filter](https://m2m.cr.usgs.gov/api/docs/datatypes/#cloudCoverFilter)
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::{SceneFilter, CloudCoverFilter};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let mut scene_filter = SceneFilter::new();
    ///    let cloud_filter = CloudCoverFilter::new(0, 100, true);
    ///    scene_filter.cloud_cover_filter(cloud_filter);
    /// #     Ok(())
    /// # }
    pub fn cloud_cover_filter(&mut self, filter: CloudCoverFilter) -> &mut Self {
        self.cloud_cover_filter = Some(filter);
        return self;
    }

    /// Add or change a dataset name filter
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::SceneFilter;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let mut scene_filter = SceneFilter::new();
    ///    scene_filter.dataset_name("lsr_landsat_8_c1");
    /// #     Ok(())
    /// # }
    pub fn dataset_name(&mut self, name: &str) -> &mut Self {
        self.dataset_name = Some(name.to_owned());
        return self;
    }

    /// Add or change an [ingest filter](https://m2m.cr.usgs.gov/api/docs/datatypes/#ingestFilter)
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::{SceneFilter, TemporalFilter};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let mut scene_filter = SceneFilter::new();
    ///    let filter = TemporalFilter::new("2018-11-30", "2018-12-30").unwrap();
    ///    scene_filter.ingest_filter(filter);
    /// #     Ok(())
    /// # }
    pub fn ingest_filter(&mut self, filter: TemporalFilter) -> &mut Self {
        self.ingest_filter = Some(filter);
        return self;
    }

    /// Add or change a [metadata filter](https://m2m.cr.usgs.gov/api/docs/datatypes/#metadataFilter)
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::{SceneFilter, MetadataFilter};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let mut scene_filter = SceneFilter::new();
    ///    let meta1 = MetadataFilter::between("5e7c418226384db5", 10, 50);
    ///    let meta2 = MetadataFilter::between("5e7c4182b7148b69", 50, 90);
    ///    let meta_filter = MetadataFilter::and(vec![meta1, meta2]);
    ///    scene_filter.metadata_filter(meta_filter);
    /// #     Ok(())
    /// # }
    pub fn metadata_filter(&mut self, filter: MetadataFilter) -> &mut Self {
        self.metadata_filter = Some(filter);
        return self;
    }

    /// Add or change a seasonal filter described by a vector of month values
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::SceneFilter;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let mut scene_filter = SceneFilter::new();
    ///    let months = vec![5,6,7];
    ///    scene_filter.seasonal_filter(months);
    /// #     Ok(())
    /// # }
    pub fn seasonal_filter(&mut self, filter: Vec<u8>) -> &mut Self {
        self.seasonal_filter = Some(filter);
        return self;
    }

    /// Add or change a [spatial filter](https://m2m.cr.usgs.gov/api/docs/datatypes/#spatialFilter)
    ///
    /// # Example
    ///
    /// ```
    /// # use std::path::Path;
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::{SceneFilter, SpatialFilter, Coordinate};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let mut scene_filter = SceneFilter::new();
    ///    let lower_left = Coordinate::new(-10.5, 10.5)?;
    ///    let upper_right = Coordinate::new(-10.5, 10.5)?;
    ///    let filter = SpatialFilter::mbr(lower_left, upper_right);
    ///    scene_filter.spatial_filter(filter);
    /// #     Ok(())
    /// # }
    pub fn spatial_filter(&mut self, filter: SpatialFilter) -> &mut Self {
        self.spatial_filter = Some(filter);
        return self;
    }
}
