use crate::error::{Error, Result};
use crate::types::GeoJson;
use serde::{Deserialize, Serialize};

/// A spatial filter
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(tag = "filterType")]
pub enum SpatialFilter {
    Mbr(SpatialFilterMbr),
    Geojson(SpatialFilterGeojson),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SpatialFilterGeojson {
    geo_json: GeoJson,
}

impl SpatialFilter {
    /// Construct a spatial filter by giving coordinate values
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::Result;
    /// # use usgs_eros_client::types::{SpatialFilter, Coordinate};
    /// #
    /// # fn main() -> Result<()> {
    /// let lower_left = Coordinate::new(-10.5, -10.6)?;
    /// let upper_right = Coordinate::new(10.6, 10.8)?;
    /// let spatial_filter = SpatialFilter::mbr(lower_left, upper_right);
    /// #
    /// # Ok(())
    /// # }
    /// ```
    pub fn mbr(lower_left: Coordinate, upper_right: Coordinate) -> SpatialFilter {
        let filter = SpatialFilterMbr {
            lower_left,
            upper_right,
        };
        SpatialFilter::Mbr(filter)
    }

    /// Construct a spatial filter from a geojson
    ///
    /// # Example
    ///
    /// ```
    /// # use std::path::Path;
    /// # use usgs_eros_client::Result;
    /// # use usgs_eros_client::types::{SpatialFilter, GeoJson};
    /// #
    /// # fn main() -> Result<()> {
    /// let geojson = GeoJson::from_file(Path::new("test-data/geojson-filter.json")).unwrap();
    /// let spatial_filter = SpatialFilter::geojson(geojson);
    /// #
    /// # Ok(())
    /// # }
    /// ```
    pub fn geojson(geojson: GeoJson) -> SpatialFilter {
        SpatialFilter::Geojson(SpatialFilterGeojson { geo_json: geojson })
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SpatialFilterMbr {
    lower_left: Coordinate,
    upper_right: Coordinate,
}

/// A named coordinate
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Coordinate {
    latitude: f64,
    longitude: f64,
}

impl Coordinate {
    /// Convenience constructor
    ///
    /// # Errors
    ///
    /// Returns a [CoordinateError](enum.Error.html#variant.EnvironmentError) in case
    /// latitude or longitude are outside of the expected bounds: (-180.0, 180.0)
    /// and (-90.0, 90.0) respectively.
    pub fn new(latitude: f64, longitude: f64) -> Result<Coordinate> {
        if (latitude < -180.0) | (latitude > 180.0) {
            return Err(Error::CoordinateError("latitude".to_owned(), latitude));
        }
        if (longitude < -90.0) | (longitude > 90.0) {
            return Err(Error::CoordinateError("longitude".to_owned(), longitude));
        }

        Ok(Coordinate {
            latitude,
            longitude,
        })
    }
}
