use std::str::FromStr;

use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

use crate::error::Result;

/// A [DateRange](https://m2m.cr.usgs.gov/api/docs/datatypes) object. Date
/// can be represented both by a date object, or by a string.
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DateRange {
    pub start_date: DateLike,
    pub end_date: DateLike,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum DateLike {
    Date(NaiveDate),
    Str(String),
}

impl DateRange {
    /// Construct a date range from ISO 8601 formatted time strings
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::Result;
    /// # use usgs_eros_client::types::DateRange;
    /// #
    /// # fn main() -> Result<()> {
    /// let temporal_filter = DateRange::new("2010-12-30", "2011-12-30")?;
    /// #
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// # Errors
    ///
    /// The constructor errors in case the provided strings can not be parsed into dates.
    pub fn new(start_date: &str, end_date: &str) -> Result<DateRange> {
        let start_date = DateLike::Date(NaiveDate::from_str(start_date)?);
        let end_date = DateLike::Date(NaiveDate::from_str(end_date)?);
        Ok(DateRange {
            start_date,
            end_date,
        })
    }
}
