use serde::{Deserialize, Serialize};

/// A cloud cover filter
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CloudCoverFilter {
    min: u8,
    max: u8,
    include_unknown: bool,
}

impl CloudCoverFilter {
    /// Instantiate a cloud cover filter
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::CloudCoverFilter;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let cloud_filter = CloudCoverFilter::new(0, 100, true);
    /// #     Ok(())
    /// # }
    pub fn new(min: u8, max: u8, include_unknown: bool) -> CloudCoverFilter {
        CloudCoverFilter {
            min,
            max,
            include_unknown,
        }
    }
}
