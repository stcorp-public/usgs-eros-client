use serde::{Deserialize, Serialize};

use crate::error::{Error, Result};

/// A metadata filter. See [docs](https://m2m.cr.usgs.gov/api/docs/datatypes/#metadataFilter)
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(tag = "filterType")]
pub enum MetadataFilter {
    And(MetadataAndOrFilter),
    Between(MetadataBetweenFilter),
    Or(MetadataAndOrFilter),
    Value(MetadataValueFilter),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MetadataAndOrFilter {
    child_filters: Vec<MetadataFilter>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MetadataBetweenFilter {
    filter_id: String,
    first_value: i64,
    second_value: i64,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MetadataValueFilter {
    filter_id: String,
    value: String,
    operand: String,
}

impl MetadataFilter {
    /// Construct a metadata 'and' filter
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::MetadataFilter;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let meta1 = MetadataFilter::between("5e7c418226384db5", 10, 50);
    ///    let meta2 = MetadataFilter::between("5e7c4182b7148b69", 50, 90);
    ///    let meta_filter = MetadataFilter::and(vec![meta1, meta2]);
    /// #     Ok(())
    /// # }
    pub fn and(child_filters: Vec<MetadataFilter>) -> MetadataFilter {
        MetadataFilter::And(MetadataAndOrFilter { child_filters })
    }

    /// Construct a metadata 'or' filter
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::MetadataFilter;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let meta1 = MetadataFilter::between("5e7c418226384db5", 10, 50);
    ///    let meta2 = MetadataFilter::between("5e7c4182b7148b69", 50, 90);
    ///    let meta_filter = MetadataFilter::or(vec![meta1, meta2]);
    /// #     Ok(())
    /// # }
    pub fn or(child_filters: Vec<MetadataFilter>) -> MetadataFilter {
        MetadataFilter::Or(MetadataAndOrFilter { child_filters })
    }

    /// Construct a metadata 'between' filter
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::MetadataFilter;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let meta1 = MetadataFilter::between("5e7c418226384db5", 10, 50);
    /// #     Ok(())
    /// # }
    pub fn between(filter_id: &str, first_value: i64, second_value: i64) -> MetadataFilter {
        MetadataFilter::Between(MetadataBetweenFilter {
            filter_id: filter_id.to_owned(),
            first_value,
            second_value,
        })
    }

    /// Construct a metadata 'value' filter
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_eros_client::{Client, Result};
    /// use usgs_eros_client::types::MetadataFilter;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let meta1 = MetadataFilter::value("5e7c418226384db5", "asd", "like");
    /// #     Ok(())
    /// # }
    pub fn value(filter_id: &str, value: &str, operand: &str) -> Result<MetadataFilter> {
        let operand = match operand {
            "=" => operand.to_owned(),
            "like" => operand.to_owned(),
            _ => {
                return Err(Error::ValidationError(
                    "Metadata value operand can be either '=' or 'like'.".to_owned(),
                ))
            }
        };

        Ok(MetadataFilter::Value(MetadataValueFilter {
            filter_id: filter_id.to_owned(),
            value: value.to_owned(),
            operand,
        }))
    }
}
