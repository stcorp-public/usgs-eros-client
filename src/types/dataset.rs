use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

use crate::types::SpatialBounds;

/// A [dataset](https://m2m.cr.usgs.gov/api/docs/datatypes/#dataset) structure
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Dataset {
    pub abstract_text: Option<String>,
    pub acquisition_start: NaiveDate,
    pub acquisition_end: Option<NaiveDate>,
    pub catalogs: Vec<String>,
    pub collection_name: String,
    pub collection_long_name: String,
    pub dataset_id: String,
    pub dataset_alias: String,
    pub dataset_category_name: String,
    pub data_owner: String,
    pub date_updated: String,
    pub doi_number: Option<String>,
    pub ingest_frequency: Option<String>,
    pub keywords: String,
    pub legacy_id: Option<i64>,
    pub scene_count: i64,
    pub spatial_bounds: Option<SpatialBounds>,
    pub temporal_coverage: Option<String>,
    pub support_cloud_cover: bool,
    pub support_deletion_search: bool,
}
