//! Types needed for working with the API, as described in [API documentation](https://m2m.cr.usgs.gov/api/docs/datatypes/)
//!
use serde::{Deserialize, Serialize};

use crate::endpoints::{DatasetCoverageResult, SceneSearchResult};

mod cloud_cover_filter;
mod credentials;
mod dataset;
mod date_range;
mod metadata_filter;
mod scene;
mod scene_filter;
mod spatial_bounds;
mod spatial_filter;
mod temporal_filter;

pub use cloud_cover_filter::CloudCoverFilter;
pub use credentials::Credentials;
pub use dataset::Dataset;
pub use date_range::{DateLike, DateRange};
pub use metadata_filter::MetadataFilter;
pub use scene::Scene;
pub use scene_filter::SceneFilter;
pub use spatial_bounds::{GeoJson, SpatialBounds};
pub use spatial_filter::{Coordinate, SpatialFilter};
pub use temporal_filter::TemporalFilter;

/// A generic API response
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ApiResponse {
    pub request_id: u64,
    pub version: String,
    pub data: Option<ApiData>,
    pub error_code: Option<String>,
    pub error_message: Option<String>,
    pub session_id: u64,
}

/// The possible contents of the `data` field of an API response
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiData {
    Token(String),
    DatasetCoverage(DatasetCoverageResult),
    Dataset(Dataset),
    Datasets(Vec<Dataset>),
    SceneSearch(SceneSearchResult),
}

/// Result sorting direction
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum SortDirection {
    ASC,
    DESC,
}

/// Metadata type
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum MetadataType {
    Summary,
    Full,
}
