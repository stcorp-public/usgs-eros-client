use chrono::ParseError as ChronoParseError;
use reqwest::header::{InvalidHeaderName, InvalidHeaderValue};
use reqwest::Error as ReqwestError;
use serde_json::Error as SerdeJsonError;
use std::io::Error as IOError;
use thiserror::Error as ThisError;
use url::ParseError;

/// Error type of this crate
#[derive(Debug, ThisError)]
pub enum Error {
    #[error("HTTP header value error")]
    HeaderValueError(#[from] InvalidHeaderValue),
    #[error("HTTP header name error")]
    HeaderNameError(#[from] InvalidHeaderName),
    #[error("URL parse error")]
    UrlParseError(#[from] ParseError),
    #[error("Request error")]
    RequestError(#[from] ReqwestError),
    #[error("Time parse error.")]
    ChronoError(#[from] ChronoParseError),
    #[error("IO error")]
    IOError(#[from] IOError),
    #[error("JSON serialization/deserialization error")]
    SerdeJsonError(#[from] SerdeJsonError),
    #[error("Could not read environment variable: {0}")]
    EnvironmentError(String),
    #[error("{0} {1} out of bounds.")]
    CoordinateError(String, f64),
    #[error(
        "Endpoint {url} yielded unexpected response: 
 {response}"
    )]
    UnexpectedResponse {
        response: String,
        url: String,
        source: Option<SerdeJsonError>,
    },
    #[error("{0}")]
    ValidationError(String),
}

/// Result type of this crate
pub type Result<T> = std::result::Result<T, Error>;
