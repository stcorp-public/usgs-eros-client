//! Crate constants
//!

/// Defines the environment variable for the USGS username
pub const ENVVAR_USER: &str = "USGS_EROS_USER";
/// Defines the environment variable for the USGS password
pub const ENVVAR_PASS: &str = "USGS_EROS_PASS";
pub const USERAGENT: &str = "usgs-eros-client";
/// Defines the USGS EROS M2M API url to use
// The code is actually not dead, the constant is being used, it's
// just behind a compile flag to ease testing with a mock server
#[allow(dead_code)]
pub const USGS_API_URL: &str = "https://m2m.cr.usgs.gov/api/api/json/stable/";
#[doc(hidden)]
pub const TOKEN_HEADER_NAME: &str = "X-Auth-Token";
