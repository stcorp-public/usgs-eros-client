# Usgs EROS M2M API Client

An idiomatic Rust API for using the [USGS EROS M2M](https://m2m.cr.usgs.gov/api/docs/json/#section-overview) data search and retrieval interface.

## Usage

Cargo.toml:

```toml
[dependencies]
usgs-eros-client = "^0.5"
tokio = {version = "^0.2", features = ["macros"]}
```

Basic functionality:

```rust
use usgs_eros_client::{Client, Result};
use usgs_eros_client::types::Credentials;
use usgs_eros_client::endpoints::DatasetRequestBuilder;

#[tokio::main]
async fn main() -> Result<()> {
    let credentials = Credentials::from_env()?;
    let client = Client::new(&credentials).await?;
    let dataset = client.dataset()
        .name("gls_all").call().await?;
    println!("Dataset response: {:?}", dataset);
    Ok(())
}
```

![](st_logo.svg)

Made by: [S&T Norway](https://www.stcorp.no)
